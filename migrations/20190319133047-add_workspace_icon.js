'use strict';
const { field } = require('app/migrations')

module.exports = {
  up: (Schema) => {
    return Schema.addColumn('workspaces', 'icon', field.string({allowNull: true}))
  },

  down: (Schema) => {
    return Schema.removeColumn('workspaces', 'icon')
  }
};
