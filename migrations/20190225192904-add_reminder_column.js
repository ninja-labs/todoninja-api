'use strict';
const { field } = require('app/migrations')

module.exports = {
  up: (Schema) => {
    return Schema.addColumn('tasks', 'remindAt', field.timestamp({ allowNull: true }))
  },

  down: (Schema) => {
    return Schema.removeColumn('tasks', 'remindAt')
  }
};
