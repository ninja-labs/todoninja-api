'use strict';
const { field } = require('app/migrations')

module.exports = {
  up: (Schema, { STRING, INTEGER, DATE }) => {
    return Schema.changeColumn('users', 'email', field.string({ unique: true }))
  },

  down: (Schema) => {
    return Schema.changeColumn('users', 'email', field.string({ unique: false }))
  }
};
