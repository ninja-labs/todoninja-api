'use strict';
const { field } = require('app/migrations')

module.exports = {
  up: (Schema) => {
    return Schema.createTable('stats', {
      ...field.id(),

      type: field.string(),
      data: field.text(),

      ...field.timestamps()
    })
  },

  down: (Schema) => {
    return Schema.dropTable('stats')
  }
};
