'use strict';
const { field } = require('app/migrations')

module.exports = {
  up: (Schema) => {
    return Schema.addColumn('tasks', 'remindedAt', field.timestamp({ allowNull: true }))
  },

  down: (Schema) => {
    return Schema.removeColumn('tasks', 'remindedAt')
  }
};
