'use strict';
const { field } = require('app/migrations')

module.exports = {
  up: (Schema) => {
    return Schema.createTable('notifications', {
      ...field.id(),

      notifiableType: field.string(),
      notifiableId: field.foreignKey(),
      data: field.text(),
      readAt: field.timestamp(),

      ...field.timestamps()
    })
  },

  down: (Schema) => {
    return Schema.dropTable('notifications')
  }
};
