'use strict';
const { field } = require('app/migrations')

module.exports = {
  up: (Schema) => {
    return Schema.createTable('push_subscriptions', {
      ...field.id(),

      userId: field.foreignKey(),
      endpoint: field.text(),
      public_key: field.string(),
      auth_token: field.string(),

      ...field.timestamps()
    })
  },

  down: (Schema) => {
    return Schema.dropTable('push_subscriptions')
  }
};
