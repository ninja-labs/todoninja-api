// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const field = require('app/migrations/field');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const workspaces = sequelizeClient.define('workspaces', {
    name: field.string(),
    userId: field.foreignKey(),
    color: field.string({ allowNull: true })
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  workspaces.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return workspaces;
};
