// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const field = require('app/migrations/field');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const pushSubscriptions = sequelizeClient.define('push_subscriptions', {
    endpoint: field.text(),
    public_key: field.string(),
    auth_token: field.string(),
    userId: field.foreignKey()
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  pushSubscriptions.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return pushSubscriptions;
};
