// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const { field } = require('app/migrations');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const stats = sequelizeClient.define('stats', {

    type: field.string(),
    data: field.text(),

  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  stats.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return stats;
};
