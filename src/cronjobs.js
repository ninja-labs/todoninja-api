const nodecron = require('node-cron');
const logger = require('./logger');

module.exports = function(app) {

  nodecron.schedule('* * * * *', async () => {
    const from = new Date();
    const to = new Date();
    from.setHours(to.getHours() - 1);

    let toremind = (await app.service('tasks').find({ query: { remindAt: { $ne: null, $lte: to, $gt: from } }, $nostats: true })).data;

    toremind = toremind.filter(task => {
      return task.remindAt > task.remindedAt;
    });

    // console.log('# Minute: ' + from.getMinutes());

    for(const task of toremind) {
      const userId = task.userId;
      const pushsubscriptions = (await app.service('push-subscriptions').find({ query: { userId } })).data;
      const notification = {
        title: task.title,
        body: 'Reminder',
        data: {
          link: {
            type: 'tasks',
            id: task.id
          }
        }
      };

      for(const subscription of pushsubscriptions) {
        // console.log('sending reminder for ' + task.title);
        app.webpush.sendNotification(
          {
            endpoint: subscription.endpoint,
            keys: { auth: subscription.auth_token,
              p256dh: subscription.public_key }
          },
          JSON.stringify(notification)
        )
          .catch(logger.error);
      }

      app.service('tasks').patch(task.id, { id: task.id, remindedAt: new Date() }, { $nostats: true });
    }
  });

};
