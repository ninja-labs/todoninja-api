const scheduler = require('./services/stats/scheduler');

module.exports = function(app) {

  const files = [];

  for(const servicename in app.services) {
    try {
      files.push({servicename, handler: require(`./services/${servicename}/${servicename}.stats`)});
    } catch(e) {
      // eslint-disable-next-line no-empty
    }
  }

  for(const file of files) {

    file.handler({app, schedule: scheduler(file.servicename)});

  }

};
