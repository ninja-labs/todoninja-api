const drivers = {
  database: require('./drivers/database.js')
};

module.exports = class Notification {
  constructor() {

  }

  via() {
    return [];
  }

  async send(to = []) {
    for(const notifiable of to) {
      const via = await this.via(notifiable);

      if(via.includes('database')) {
        await drivers.database.send(this, notifiable);
      }
    }
  }
};
