const app = require('./../../app');

module.exports = {
  send(notification, notifiable) {
    const entry = {
      type: notification.constructor.name,
      notifiable_type: notifiable.type,
      notifiable_id: notifiable.id,
      data: notification.data(notifiable)
    };

    // console.log(app);

    return app.service('notifications').create(entry);
  }
};
