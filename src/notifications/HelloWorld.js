const Notification = require('./Notification');

module.exports = class HelloWorld extends Notification {
  constructor(message) {
    super();

    this.message = message;
  }

  via() {
    return [ 'database' ];
  }

  data(notifiable) {
    return {
      message: `Hello ${notifiable.type}, ${this.message}.`,
    };
  }
};
