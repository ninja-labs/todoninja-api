const { count } = require('../stats/types');

module.exports = function({ schedule }) {

  schedule('*/5 * * * *', [

    count(),

  ]);

};
