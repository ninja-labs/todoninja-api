let app;

module.exports = {

  setup(newapp) {
    app = newapp;
  },

  count(options = {}) {
    options = Object.assign({
      label: 'count',
      query: {}
    }, options);

    options.query.$limit = 0;

    return async service => {

      app.service('stats').create({
        type: service + ':' + options.label,
        data: (await app.service(service).find({ query: options.query, $nostats: true })).total
      });

    };
  }

};
