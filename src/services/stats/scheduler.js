const nodecron = require('node-cron');

// const units = 'second minute hour day month'.split(' ')
// const spans = [
//   { word: 'one', value: '*' },
//   { word: 'two', value: '*/2' },
//   { word: 'three', value: '*/3' },
//   { word: 'four', value: '*/4' },
//   { word: 'five', value: '*/5' },
// ]

// function compose(words) {
//   let result = ''
//   for(const word of words) {
//     if(result.length < 1) {
//       result += word
//     } else {
//       result += word.charAt(0).toUpperCase() + word.slice(1)
//     }
//   }
// }

// const schedules = {}

// for(const unit of units) {
//   for(const span of spans) {
//     const name = 'every' + unit.toUpperCase
//     schedules[compose([ 'every', span.word, unit ])]
//   }
// }

module.exports = function(servicename) {
  return function(cron, jobs) {
    return nodecron.schedule(cron, async () => {
      return await Promise.all(jobs.map(job => job(servicename)));
    });
  };
};
