const { count } = require('../stats/types');

module.exports = function({ schedule }) {

  schedule('*/5 * * * *', [

    count(),
    count({
      query: { doneAt: { $ne: null } },
      label: 'done'
    }),
    count({
      query: { doneAt: null },
      label: 'open'
    })

  ]);

};
