const { authenticate } = require('@feathersjs/authentication').hooks;
const authorize = require('app/hooks/authorize');
const validate = require('app/hooks/validate');

module.exports = {
  before: {
    all: [ authenticate('jwt'), authorize(), validate() ],
    find: [ ],
    get: [ ],
    create: [ ],
    update: [ ],
    patch: [ ],
    remove: [ ]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
