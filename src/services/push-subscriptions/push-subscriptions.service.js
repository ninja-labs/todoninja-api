// Initializes the `push-subscriptions` service on path `/push-subscriptions`
const createService = require('feathers-sequelize');
const createModel = require('../../models/push-subscriptions.model');
const hooks = require('./push-subscriptions.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/push-subscriptions', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('push-subscriptions');

  service.hooks(hooks);
};
