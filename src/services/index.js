const users = require('./users/users.service.js');
const tasks = require('./tasks/tasks.service.js');
const workspaces = require('./workspaces/workspaces.service.js');
const tags = require('./tags/tags.service.js');
const pushSubscriptions = require('./push-subscriptions/push-subscriptions.service.js');
const notifications = require('./notifications/notifications.service.js');
const stats = require('./stats/stats.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(tasks);
  app.configure(workspaces);
  app.configure(tags);
  app.configure(pushSubscriptions);
  app.configure(notifications);
  app.configure(stats);
};
