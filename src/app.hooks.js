// Application hooks that run for every service
const log = require('./hooks/log');

module.exports = {
  before: {
    all: [ log(), ({ app, path, method, params }) => {
      if(path != 'stats' && !params.$nostats) app.service('stats').create({ type: 'endpointhit:' + path, data: { method } });
    } ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [ log() ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [ log() ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
