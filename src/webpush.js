const webpush = require('web-push');

module.exports = {
  setup(app) {
    webpush.setVapidDetails(
      'mailto:example@yourdomain.org',
      app.get('webpush').vapid.public,
      app.get('webpush').vapid.private
    );

    app.webpush = webpush;
  },

  webpush
};


