const assert = require('assert');
const app = require('../../src/app');

describe('\'push-subscriptions\' service', () => {
  it('registered the service', () => {
    const service = app.service('push-subscriptions');

    assert.ok(service, 'Registered the service');
  });
});
